import requests
import json
import numpy as np
from influxdb import InfluxDBClient

start = "2020-04-05T00:00:00Z"
end = "2020-05-17T00:00:00Z"
country = "russia"
query = f"https://api.covid19api.com/dayone/country/{country}/status/confirmed?from={start}&to={end}"
resp = requests.get(query)
content = json.loads(resp.content)

client = InfluxDBClient(host='127.0.0.1', port=8086, 
                        username='root', password='root', database='covid_devops')

cases = np.array([x['Cases'] for x in content])
increments = cases[1:] - cases[:-1]

points = [{
    'time': y['Date'],
    'measurement': 'daily_cases',
    'fields': {
       'cases_per_day': x,
       'total_cases': y['Cases']
    }
} for x, y in zip(increments, content[1:])]

client.write_points(points)